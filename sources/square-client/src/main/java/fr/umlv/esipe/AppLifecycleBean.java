package fr.umlv.esipe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class AppLifecycleBean {

	private static final Logger LOGGER = LoggerFactory.getLogger("ListenerBean");

	@Inject
	@RestClient
	ClientService clientService;

	@ConfigProperty(name = "application.name")
	String appName;
	@ConfigProperty(name = "application.id")
	String appId;

	void onStart(@Observes StartupEvent ev) throws IOException {
		LOGGER.info("The application is starting...");
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("id", String.valueOf(appId));
		clientService.log(map);

		Process p = new ProcessBuilder(Arrays.asList("java", "-jar", "./" + appName + "/" + appName + ".jar")).start();
		String line;
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		while ((line = input.readLine()) != null) {
			map.put("message", line);
			clientService.log(map);
		}
		input.close();
	}

	void onStop(@Observes ShutdownEvent ev) {
		LOGGER.info("The application is stopping...");
	}
}