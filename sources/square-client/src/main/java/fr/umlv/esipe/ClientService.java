package fr.umlv.esipe;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import java.util.Map;

@Path("/client")
@RegisterRestClient
public interface ClientService {

	@POST
	@Path("/log")
	@Consumes("application/json")
	public void log(Map<String, String> map);
}