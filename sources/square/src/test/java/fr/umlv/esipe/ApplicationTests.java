package fr.umlv.esipe;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;

import java.nio.file.Path;
import java.nio.file.Paths;

import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
public class ApplicationTests {
	
	@Test
    @Order(1)
    public void deployAmazon() {
		System.out.println(Path.of("."));
    	given()
	        .body("{\"app\": \"amazon:8010\"}")
	        .header("Content-Type", MediaType.APPLICATION_JSON)
	    .when()
	        .post("/app/deploy")
	    .then()
	        .statusCode(200)
	        .body(is("{\"id\":200,\"app\":\"amazon:8010\",\"port\":8010,\"service-port\":15200,\"docker-instance\":\"amazon-0\"}"));
    }
	
	@Test
    @Order(2)
    public void listOneApp() {		
    	given()
	        .when().get("/app/list")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(1))
	           .body("id", containsInAnyOrder(200))
	           .body("app", containsInAnyOrder("amazon:8010"))
	           .body("port", containsInAnyOrder(8010))
	           .body("service-port", containsInAnyOrder(15200))
	           .body("docker-instance", containsInAnyOrder("amazon-0"));
    }
	
	@Test
    @Order(3)
    public void deployMicrosoft() {    	
    	given()
	         .body("{\"app\": \"microsoft:8020\"}")
	         .header("Content-Type", MediaType.APPLICATION_JSON)
	     .when()
	         .post("/app/deploy")
	     .then()
	         .statusCode(200)
	         .body(is("{\"id\":201,\"app\":\"microsoft:8020\",\"port\":8020,\"service-port\":15201,\"docker-instance\":\"microsoft-0\"}"));
    }
	
	@Test
    @Order(4)
    public void listTwoApp() {		
    	given()
	        .when().get("/app/list")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(2))
	           .body("id", containsInAnyOrder(200, 201))
	           .body("app", containsInAnyOrder("amazon:8010", "microsoft:8020"))
	           .body("port", containsInAnyOrder(8010, 8020))
	           .body("service-port", containsInAnyOrder(15200, 15201))
	           .body("docker-instance", containsInAnyOrder("amazon-0", "microsoft-0"));
    }
	
    @Test
    @Order(5)
    public void deployGoogle() {
    	given()
	        .body("{\"app\": \"google:8030\"}")
	        .header("Content-Type", MediaType.APPLICATION_JSON)
	    .when()
	        .post("/app/deploy")
	    .then()
	        .statusCode(200)
	        .body(is("{\"id\":202,\"app\":\"google:8030\",\"port\":8030,\"service-port\":15202,\"docker-instance\":\"google-0\"}"));
    }
    
    @Test
    @Order(6)
    public void listThreeApps() {		
    	given()
	        .when().get("/app/list")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(3))
	           .body("id", containsInAnyOrder(200, 201, 202))
	           .body("app", containsInAnyOrder("amazon:8010", "microsoft:8020", "google:8030"))
	           .body("port", containsInAnyOrder(8010, 8020, 8030))
	           .body("service-port", containsInAnyOrder(15200, 15201, 15202))
	           .body("docker-instance", containsInAnyOrder("amazon-0", "microsoft-0", "google-0"));
    }
        
    @Test
    @Order(7)
    public void stopAmazon() {
    	given()
	        .body("{\"id\": \"200\"}")
	        .header("Content-Type", MediaType.APPLICATION_JSON)
	    .when()
	        .post("/app/stop")
	    .then()
	        .statusCode(200)
	        .body("id", is(200))
	        .body("app", is("amazon:8010"))
	        .body("port", is(8010))
	        .body("service-port", is(15200))
	        .body("docker-instance", is("amazon-0"));
	}
    
    @Test
    @Order(8)
    public void listTwoAppsAgain() {		
    	given()
	        .when().get("/app/list")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(2))
	           .body("id", containsInAnyOrder(201, 202))
	           .body("app", containsInAnyOrder("microsoft:8020", "google:8030"))
	           .body("port", containsInAnyOrder(8020, 8030))
	           .body("service-port", containsInAnyOrder(15201, 15202))
	           .body("docker-instance", containsInAnyOrder("microsoft-0", "google-0"));
    }
    
    @Test
    @Order(9)
    public void stopGoogle() {
    	given()
	        .body("{\"id\": \"202\"}")
	        .header("Content-Type", MediaType.APPLICATION_JSON)
	    .when()
	        .post("/app/stop")
	    .then()
	        .statusCode(200)
	        .body("id", is(202))
	        .body("app", is("google:8030"))
	        .body("port", is(8030))
	        .body("service-port", is(15202))
	        .body("docker-instance", is("google-0"));
	}
    
    @Test
    @Order(10)
    public void listOneAppAgain() {		
    	given()
	        .when().get("/app/list")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(1))
	           .body("id", containsInAnyOrder(201))
	           .body("app", containsInAnyOrder("microsoft:8020"))
	           .body("port", containsInAnyOrder(8020))
	           .body("service-port", containsInAnyOrder(15201))
	           .body("docker-instance", containsInAnyOrder("microsoft-0"));
    }
    
    @Test
    @Order(11)
    public void stopMicrosoft() {
    	given()
	        .body("{\"id\": \"201\"}")
	        .header("Content-Type", MediaType.APPLICATION_JSON)
	    .when()
	        .post("/app/stop")
	    .then()
	        .statusCode(200)
	        .body("id", is(201))
	        .body("app", is("microsoft:8020"))
	        .body("port", is(8020))
	        .body("service-port", is(15201))
	        .body("docker-instance", is("microsoft-0"));
	}
    
    @Test
    @Order(12)
    public void listNoApps() {		
    	given()
	        .when().get("/app/list")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(0));
    }
    
}














