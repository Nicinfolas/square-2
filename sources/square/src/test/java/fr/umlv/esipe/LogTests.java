package fr.umlv.esipe;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.stream.Stream;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
public class LogTests {
	
	private static int k = 1;
	
	@Test
	@Order(1)
    public void tryToGetEveryLogs() throws SQLException {
		
		LogManager.dropTable();
		LogManager.createTable();
		
		for (int i = 1; i < 11; i++) {
			LogManager.insertLog(new Log(k, "amazon:8080", 8080, 4040, "amazon-1", "Message n°"+ i +".", Timestamp.from(Instant.now()).toString()));
			k++;
		}			
		
		for (int j = 1; j < 11; j++) {
			LogManager.insertLog(new Log(k, "amazon:8080", 8080, 6060, "amazon-2", "Message n°"+j+".", Timestamp.from(Instant.now().minusSeconds(60 * 10 * j)).toString()));
			k++;
		}			
		for (int j = 1; j < 6; j++) {
			LogManager.insertLog(new Log(k, "microsoft:8080", 8080, 6060, "microsoft-1", "Message n°"+j+".", Timestamp.from(Instant.now().minusSeconds(60 * 300)).toString()));
			k++;
		}
		 
		given()
	        .when().get("/logs/1000000")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(25));
	     
	}				  
	
	@Test
	@Order(2)
    public void tryToGetLast5MinutesLogs() {
    	given()
	        .when().get("/logs/5")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(10));
    	}
	
	@Test
	@Order(3)
    public void tryToGetLast45MinutesLogs() {
    	given()
	        .when().get("/logs/45")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(14));
	}
	
	@Test
	@Order(4)
    public void tryToGetLast150MinutesLogs() {
    	given()
	        .when().get("/logs/150")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(20));
	}
		
	@Test
	@Order(5)
    public void tryToGetFilteredInstanceLogs1() {
    	given()
	        .when().get("/logs/150/amazon-1")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(10), "service-port", containsInAnyOrder(4040, 4040, 4040, 4040, 4040, 4040, 4040, 4040, 4040, 4040));
	}
	
	@Test
	@Order(6)
    public void tryToGetFilteredInstanceLogs2() {
    	given()
	        .when().get("/logs/150/amazon-2")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(10), "service-port", containsInAnyOrder(6060, 6060, 6060, 6060, 6060, 6060, 6060, 6060, 6060, 6060));
	}
	
	@Test
	@Order(7)
    public void tryToGetFilteredNameLogs1() {
    	given()
	        .when().get("/logs/150/amazon:8080")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(20));
    }
	
	@Test
	@Order(8)
    public void tryToGetFilteredNameLogs2() {
    	given()
	        .when().get("/logs/150/microsoft:8080")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(0));
    }
	
	@Test
	@Order(9)
    public void tryToGetFilteredNameLogs3() {
    	given()
	        .when().get("/logs/200/microsoft:8080")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(0));
    }
	
	@Test
	@Order(10)
    public void tryToGetFilteredNameLogs4() {
    	given()
	        .when().get("/logs/400/microsoft:8080")
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(5));
    }
	
	@ParameterizedTest
	@Order(11)
	@MethodSource("getIDs")
    public void tryToGetFilteredIDLogs(int id) {
		given()
	        .when().get("/logs/1000/" + id)
	        .then()
	           .statusCode(200)
	           .body("$.size()", is(1));
    }
	
	private static Stream<Integer> getIDs() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i = 1; i < k; i++)
			list.add(i);
		return list.stream();
	}
	
}




















