package fr.umlv.esipe;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbPropertyOrder;

@JsonbPropertyOrder({ "id", "app", "port", "service-port", "docker-instance", "message", "timestamp"})
public class Log {
	
	@JsonbProperty("id")
	private final int id;
	@JsonbProperty("app")
	private final String app;
	@JsonbProperty("port")
	private final int port;
	@JsonbProperty("service-port")
	private final int servicePort;
	@JsonbProperty("docker-instance")
	private final String name;
	@JsonbProperty("message")
	private final String message;
	@JsonbProperty("timestamp")
	private final String date;
	
	public Log(int id, String app, int port, int servicePort, String name, String message, String date) {
		this.id = id;
		this.app = app;
		this.port = port;
		this.servicePort = servicePort;
		this.name = name;
		this.message = message;
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public String getApp() {
		return app;
	}

	public int getPort() {
		return port;
	}

	public int getServicePort() {
		return servicePort;
	}

	public String getName() {
		return name;
	}

	public String getMessage() {
		return message;
	}

	public String getDate() {
		return date;
	}
	
}
