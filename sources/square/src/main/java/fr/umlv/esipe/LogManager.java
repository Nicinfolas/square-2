package fr.umlv.esipe;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LogManager {
	
	public static void dropTable() {
		execute("drop table log;");
	}
	
	public static void createTable() {		
		execute("create table log (id integer primary key not null, app string, port integer, servicePort integer, name string, message string, mydate timestamp NOT NULL);");
	}	
	
	private static void execute(String request) {		
		Connection connection = null;		
	    try {
	    	Class.forName("org.sqlite.JDBC");
	    	connection = DriverManager.getConnection("jdbc:sqlite:sample.db");
	    	Statement statement = connection.createStatement();
	    	statement.executeUpdate(request);
	    	ResultSet rs = statement.executeQuery("select * from log;");
	    	int i = 0; while (rs.next()) i++;
	    	System.out.println("lines : " + i);
	    }
	    catch(SQLException e) {
	    	System.err.println(e.getMessage());
	    }
	    catch (ClassNotFoundException e1) {
	    	throw new AssertionError();
		}
	    finally {
	    	try {
	    		if(connection != null)
	    			connection.close();
	    	}
	    	catch(SQLException e) {
	    		System.err.println(e);
	    	}
	    }
	}
	public static void insertLog(Log log) {
		Objects.requireNonNull(log);
		String request = "insert into log values("
				+ log.getId() + ", '"
				+ log.getApp() + "', "
				+ log.getPort() + ", "
				+ log.getServicePort() + ", '"
				+ log.getName() + "', '"
				+ log.getMessage() + "', '"
				+ log.getDate() + "');";
		execute(request);		
	}
	
	public static List<Log> getLogsSince(int minutes) {
		return getLogsFilteredSince(minutes, null);
	}
	
	public static ArrayList<Log> getLogsFilteredSince(int minutes, String filter) {
		if (minutes < 0) 
			throw new IllegalArgumentException();
		Connection connection = null;
		ArrayList<Log> list = new ArrayList<Log>(); 
		StringBuilder request = new StringBuilder();		
		request.append("select * from log where mydate >= '" + Timestamp.from(Instant.now().minusSeconds(60 * minutes)).toString() + "'");
		if (filter != null)	
			request.append(constructFilter(filter));
		request.append(";");		
	    try {
	    	Class.forName("org.sqlite.JDBC");
	    	connection = DriverManager.getConnection("jdbc:sqlite:sample.db");
	    	ResultSet result = connection.createStatement().executeQuery(request.toString());
	    	while(result.next())	    	
	    		list.add(new Log(result.getInt("id"), result.getString("app"), result.getInt("port"), result.getInt("servicePort"), result.getString("name"), result.getString("message"), result.getString("mydate")));
	    }
	    catch(SQLException e) {
	    	System.err.println(e.getMessage());
	    }
	    catch (ClassNotFoundException e1) {
	    	throw new AssertionError();
		}
	    finally {
	    	try {
	    		if(connection != null)
	    			connection.close();
	    	}
	    	catch(SQLException e) {
	    		System.err.println(e);
	    	}
	    }
		return list;
	}
	
	private static String constructFilter(String filter) {
		StringBuilder SB = new StringBuilder();
		SB.append(" and ");
		if (filter.contains(":")) {
			SB.append("app=");
			SB.append("'" + filter + "'");
		}			
		else if (filter.contains("-")) {
			SB.append("name=");
			SB.append("'" + filter + "'");
		}			
		else {
			SB.append("id=");
			SB.append(filter);
		}			
		return SB.toString();
	}
	
}