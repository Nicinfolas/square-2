package fr.umlv.esipe;

import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.List;

public class DockerLauncher {

	private final String name;
	private final Image image;
	private final int port;
	private final int exposedPort;

	public DockerLauncher(String name, Image image, int port, int exposedPort) {
		this.name = name;
		this.image = image;
		this.port = port;
		this.exposedPort = exposedPort;
	}

	public static DockerLauncher empty(String name) {
		return new DockerLauncher(name, null, 0, 0);
	}

	public void createContainer() throws InterruptedException, IOException {
		System.out.println("createContainer()");
		ProcessBuilder pb = new ProcessBuilder(List.of("/usr/bin/docker", "container", "create", "--name", name, "-p",
				exposedPort + ":" + port, image.getImageName()));
		pb.redirectOutput(Redirect.INHERIT);
		pb.redirectError(Redirect.INHERIT);
		Process process = pb.start();
		process.waitFor();
		process.destroy();
	}

	public void removeContainer() throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder(List.of("/usr/bin/docker", "container", "rm", name));
		pb.redirectOutput(Redirect.INHERIT);
		pb.redirectError(Redirect.INHERIT);
		Process process = pb.start();
		process.waitFor();
		process.destroy();
	}

	public void start() throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder(List.of("/usr/bin/docker", "container", "start", name));
		pb.redirectOutput(Redirect.INHERIT);
		pb.redirectError(Redirect.INHERIT);
		Process process = pb.start();
		process.waitFor();
		process.destroy();
	}

	public void stop() throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder(List.of("/usr/bin/docker", "container", "stop", name));
		pb.redirectOutput(Redirect.INHERIT);
		pb.redirectError(Redirect.INHERIT);
		Process process = pb.start();
		process.waitFor();
		process.destroy();
	}

}
