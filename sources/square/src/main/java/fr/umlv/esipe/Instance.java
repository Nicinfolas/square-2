package fr.umlv.esipe;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbPropertyOrder;
import javax.json.bind.annotation.JsonbTransient;

@JsonbPropertyOrder({ "id", "app", "port", "servicePort", "name" })
public class Instance {

	@JsonbProperty("app")
	private final String app;
	@JsonbProperty("id")
	private final int id;
	@JsonbProperty("port")
	private final int port;
	@JsonbProperty("service-port")
	private final int servicePort;
	@JsonbProperty("docker-instance")
	private final String name;
	@JsonbTransient
	private final Instant instant;
	// @JsonbTransient
	// private final String dockerId;

	public Instance(String app, int id, int port, int servicePort, String name, Instant instant) {
		this.app = app;
		this.id = id;
		this.port = port;
		this.servicePort = servicePort;
		this.name = name;
		this.instant = instant;
	}

	public static Instance createInstance(String name, int id, int port, int num) {
		String[] tab = name.split(":");
		Instance inst = new Instance(name, id, Integer.parseInt(tab[1]), port, tab[0] + "-" + num, Instant.now());
		return inst;
	}

	public String getApp() {
		return app;
	}

	public int getId() {
		return id;
	}

	public int getPort() {
		return port;
	}

	public int getServicePort() {
		return servicePort;
	}

	public String getName() {
		return name;
	}

	@JsonbTransient
	public String getElaspedTime() {
		long millis = Duration.between(instant, Instant.now()).toMillis();
		return String.format("%dm%ds", TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis)
				- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
	}

	@JsonbTransient
	public String getImageName() {
		return "square-" + app.replace(':', '-');
	}

}
