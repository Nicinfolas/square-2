package fr.umlv.esipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class InstanceList {

	private static final HashMap<String, AppInstances> allInstances = new HashMap<String, AppInstances>();

	private static class AppInstances {
		private int n;
		private ArrayList<Instance> insts = new ArrayList<Instance>();

		public void add(Instance i) {
			insts.add(i);
			n++;
		}

		public void remove(Instance i) {
			insts.remove(i);
		}

		public int count() {
			return n;
		}

		public List<Instance> list() {
			return List.copyOf(insts);
		}
	}

	private static int portCount = 15200;
	private static int idCount = 200;
	private static int ContainerCount = 0;

	public static int nextPort() {
		return portCount++;
	}

	public static int nextId() {
		return idCount++;
	}

	public static int nextContainer() {
		return ContainerCount++;
	}

	public static int nbr(String name) {
		AppInstances lst = allInstances.get(name);
		if (lst == null)
			return 0;
		return lst.count();
	}

	public static void register(Instance i) {
		String name = i.getApp().split(":")[0];
		allInstances.putIfAbsent(name, new AppInstances());
		allInstances.get(name).add(i);
	}

	public static void unregister(Instance i) {
		String name = i.getApp().split(":")[0];
		allInstances.get(name).remove(i);
	}

	public static Optional<Instance> getById(int id) {
		return allInstances.values().stream().flatMap(instances -> instances.list().stream())
				.filter(instance -> instance.getId() == id).findFirst();
	}

	public static List<InstanceTimed> allInstances() {
		System.out.println(allInstances.values());
		return allInstances.values().stream().flatMap(e -> e.list().stream()).map(e -> InstanceTimed.from(e))
				.collect(Collectors.toList());
	}
}