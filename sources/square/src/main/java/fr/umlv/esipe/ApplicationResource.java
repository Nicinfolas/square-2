package fr.umlv.esipe;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.InternalServerErrorException;

@Path("/app")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class ApplicationResource {

	@POST
	@Path("/deploy")
	public Instance deploy(Map<String, String> map) {
		System.out.println("ApplicationResource.deploy()");
		Instance i = Instance.createInstance(map.get("app"), InstanceList.nextId(), InstanceList.nextPort(),
				InstanceList.nbr(map.get("app").split(":")[0]));
		Image img = Image.fromInstance(i);
		try {
			img.createImageIfAbsent();
			DockerLauncher dc = new DockerLauncher(i.getName(), img, i.getPort(), i.getServicePort());
			dc.createContainer();
			dc.start();
		} catch (IOException | InterruptedException e) {
			throw new InternalServerErrorException(e);
		}
		InstanceList.register(i);
		return i;
	}

	@GET
	@Path("/list")
	public List<InstanceTimed> list() {
		System.out.println("ApplicationResource.list()");
		return InstanceList.allInstances();
	}

	@POST
	@Path("/stop")
	public InstanceTimed stop(Map<String, String> map) {
		System.out.println("ApplicationResource.stop()");
		Optional<Instance> instance = InstanceList.getById(Integer.parseInt(map.get("id")));
		if (instance.isEmpty())
			throw new IllegalArgumentException();
		InstanceList.unregister(instance.get());
		try {
			DockerLauncher DC = DockerLauncher.empty(instance.get().getName());
			DC.stop();
			DC.removeContainer();
		} catch (IOException | InterruptedException e) {
			throw new InternalServerErrorException(e);
		}
		return InstanceTimed.from(instance.get());
	}

}
