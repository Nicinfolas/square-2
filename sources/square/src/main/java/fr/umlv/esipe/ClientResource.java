package fr.umlv.esipe;

import java.time.Instant;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/client")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class ClientResource {

	@POST
	@Path("/log")
	public void Log(Map<String, String> map) {
		Instance i = InstanceList.getById(Integer.valueOf(map.get("id"))).orElseThrow();
		long millis = Instant.now().toEpochMilli();

		Log log = new Log(i.getId(), i.getApp(), i.getServicePort(), i.getServicePort(), i.getName(),
				map.get("message"), String.format("%dm%ds", TimeUnit.MILLISECONDS.toMinutes(millis),
						TimeUnit.MILLISECONDS.toSeconds(millis)));
		System.out.println(log + " : " + map.get("message"));
		
		// LogManager.insertLog(log);
	}
}
