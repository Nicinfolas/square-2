package fr.umlv.esipe;

import java.sql.SQLException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/logs")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LogResource {
	
    public LogResource() throws SQLException {
    	LogManager.createTable();
    }
    
    @GET
    @Path("/{time}")
    public List<Log> logsSince(@PathParam("time") String time) throws NumberFormatException, SQLException {
    	return LogManager.getLogsFilteredSince(Integer.parseInt(time), null);
    }
    
    @GET
    @Path("/{time}/{filter}")
    public List<Log> filteredLogs(@PathParam("time") String time, @PathParam("filter") String filter) throws NumberFormatException, SQLException {
    	return LogManager.getLogsFilteredSince(Integer.parseInt(time), filter);
    }
    
}






