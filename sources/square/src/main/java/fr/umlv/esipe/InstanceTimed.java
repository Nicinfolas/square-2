package fr.umlv.esipe;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbPropertyOrder;

@JsonbPropertyOrder({ "id", "app", "port", "servicePort", "name", "instant" })
public class InstanceTimed {

	@JsonbProperty("app")
	private final String app;
	@JsonbProperty("id")
	private final int id;
	@JsonbProperty("port")
	private final int port;
	@JsonbProperty("service-port")
	private final int servicePort;
	@JsonbProperty("docker-instance")
	private final String name;
	@JsonbProperty("elapsed-time")
	private final String instant;

	private InstanceTimed(String app, int id, int port, int servicePort, String name, String instant) {
		this.app = app;
		this.id = id;
		this.port = port;
		this.servicePort = servicePort;
		this.name = name;
		this.instant = instant;
	}

	public static InstanceTimed from(Instance i) {
		return new InstanceTimed(i.getApp(), i.getId(), i.getPort(), i.getServicePort(), i.getName(),
				i.getElaspedTime());
	}

	public String getApp() {
		return app;
	}

	public int getId() {
		return id;
	}

	public int getPort() {
		return port;
	}

	public int getServicePort() {
		return servicePort;
	}

	public String getName() {
		return name;
	}

	public String getInstant() {
		return instant;
	}
}
