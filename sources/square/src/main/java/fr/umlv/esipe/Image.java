package fr.umlv.esipe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;

public class Image {

	public final static Path IMAGE_DIR = Paths.get("docker-images/");
	public final static Path APPS_DIR = Paths.get("apps/");

	private final static String SQUARE_CLIENT = "square-client";

	private final String appName;
	private final int port;
	private final String imageName;
	private final int appId;

	public void createImageIfAbsent() throws IOException, InterruptedException {
		if (!exist()) {
			copyJar();
			copySquareClient();
			createDockerfile();
			createImage();
		}
	}

	public Image(String appName, int port, String imageName, int appId) {
		this.appName = appName;
		this.port = port;
		this.imageName = imageName;
		this.appId = appId;
	}

	public static Image fromInstance(Instance from) {
		return new Image(from.getApp().split(":")[0], from.getPort(), from.getImageName(), from.getId());
	}

	private boolean exist() throws IOException, InterruptedException {
		Process process = new ProcessBuilder(List.of("/usr/bin/docker", "images", imageName, "-q")).start();
		BufferedReader sysout = new BufferedReader(new InputStreamReader(process.getInputStream()));
		return sysout.readLine() != null;
	}

	private void createDockerfile() throws IOException {
		Path path = IMAGE_DIR.resolve(imageName);
		if (Files.notExists(path)) {
			Files.createDirectory(path);
		}
		path = path.resolve("Dockerfile");
		Files.deleteIfExists(path);
		Files.createFile(path);
		PrintWriter writer = new PrintWriter(path.toString(), "UTF-8");
		writer.println("FROM java");
		writer.println("WORKDIR /");
		writer.println("ADD " + appName + " " + appName);
		writer.println("ADD " + SQUARE_CLIENT + " " + SQUARE_CLIENT);
		writer.println("EXPOSE " + port);
		writer.println("CMD java -jar -Dapplication.name=" + appName + " -Dapplication.id=" + appId + " "
				+ SQUARE_CLIENT + "/" + SQUARE_CLIENT + ".jar");
		writer.println("");
		writer.close();
	}

	private void createImage() throws InterruptedException, IOException {
		Process process = new ProcessBuilder(
				List.of("/usr/bin/docker", "build", "docker-images/" + imageName, "-t", imageName)).start();

		BufferedReader inputStream = new BufferedReader(new InputStreamReader(process.getInputStream()));
		BufferedReader errorStream = new BufferedReader(new InputStreamReader(process.getErrorStream()));

		process.waitFor();

		String line = null;

		while ((line = inputStream.readLine()) != null) {
			System.out.println(line);
		}

		while ((line = errorStream.readLine()) != null) {
			System.out.println("[ERROR] " + line);
		}
	}

	private void copyJar() throws IOException {
		Files.createDirectories(IMAGE_DIR.resolve(imageName));
		Files.createDirectories(IMAGE_DIR.resolve(imageName).resolve(appName));
		for (Path p : Files.walk(APPS_DIR.resolve(appName)).collect(Collectors.toList())) {
			Files.copy(p,
					IMAGE_DIR.resolve(imageName).resolve(appName).resolve(APPS_DIR.resolve(appName).relativize(p)),
					StandardCopyOption.REPLACE_EXISTING);
		}
	}

	private void copySquareClient() throws IOException {
		Files.createDirectories(IMAGE_DIR.resolve(imageName));
		Files.createDirectories(IMAGE_DIR.resolve(imageName).resolve(SQUARE_CLIENT));
		for (Path p : Files.walk(APPS_DIR.resolve(SQUARE_CLIENT)).collect(Collectors.toList())) {
			Files.copy(p, IMAGE_DIR.resolve(imageName).resolve(SQUARE_CLIENT)
					.resolve(APPS_DIR.resolve(SQUARE_CLIENT).relativize(p)), StandardCopyOption.REPLACE_EXISTING);
		}
	}

	public String getImageName() {
		return imageName.toLowerCase();
	}

}